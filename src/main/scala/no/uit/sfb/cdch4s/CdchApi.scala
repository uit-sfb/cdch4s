package no.uit.sfb.cdch4s

import sttp.client3.{Identity, SttpBackend}
import sttp.client3._
import sttp.client3.circe._
import no.uit.sfb.cdch4s.model._
import io.circe.generic.auto._
import no.uit.sfb.cdch4s.model.response.{CurationResponse, GetResponse, PatchResponse}

class CdchApi(apiUrl: String)
             (implicit backend: SttpBackend[Identity, Any], tokenProvider: CdchTokenProvider){
   private def token = tokenProvider.token

   /** Submit curations
    *
    * @param curObj
    * @return
    */
   def create(curObj: Curations): CurationResponse = {
      assert(curObj.curations.nonEmpty, "Curation object may not be empty.")
      val request = basicRequest.auth.bearer(token)
        .post(uri"$apiUrl/curations")
        .body(curObj)

      val resp = request
        .response(asJsonAlways[CurationResponse])
        .send(backend)

      resp.body match {
         case Left(e) =>
            throw e
         case Right(cr) =>
            cr
      }
   }

   /** Submit curations
    *
    * @param curs
    * @return
    */
   def create(curs: Seq[Curation]): CurationResponse = {
      create(Curations(curs))
   }

   /** Submit stream of curations (by batch)
    *
    * @param it
    * @param batchSize
    * @return
    */
   def create(it: Iterator[Curation], batchSize: Int = 100): Iterator[CurationResponse] = {
      it.grouped(batchSize).map{grp => create(grp)}
   }

   //Fetch by record or curation id. An attribute name may optionally be provided in the first case
   protected def getById(id: String, attributeName: Option[String] = None): GetResponse = {
      val url = uri"$apiUrl/curations/$id".addParam("attributeName", attributeName)
      val request = basicRequest.get(url)

      val resp = request
        .response(asJson[GetResponse].getRight)
        .send(backend)

        resp.body
   }

   /** Fetch by record id and optionally a specific 'attributePre' or 'attributePost'.
    *
    * @param recordId
    * @param attributeName
    * @return
    */
   def getByRecordId(recordId: String, attributeName: Option[String] = None): GetResponse = {
      getById(recordId, attributeName)
   }

   /** Fetch by curation id
    *
    * @param uuid
    * @return
    */
   def getByUuid(uuid: String): GetResponse = {
      getById(uuid)
   }

   /** Fetch all records matching the given filters
    *
    * @param dataTypeIdentifier
    * @param startTime
    * @param endTime
    * @param limit
    * @param offset
    * @param providerName
    * @param recordType
    * @return
    */
   def fetch(dataTypeIdentifier: Option[String] = None,
             startTime: Option[String] = None,
             endTime: Option[String] = None,
             limit: Option[Int] = None,
             offset: Option[Int] = None,
             providerName: Option[String] = None,
             recordType: Option[String] = None
            ) = {
      val url = uri"$apiUrl/curations"
        .addParam("dataTypeIdentifier", dataTypeIdentifier)
        .addParam("startTime", startTime)
        .addParam("endTime", endTime)
        .addParam("limit", limit.map{_.toString})
        .addParam("offset", offset.map{_.toString})
        .addParam("providerName", providerName)
        .addParam("recordType", recordType)
      val request = basicRequest.get(url)

      val resp = request
        .response(asJson[GetResponse].getRight)
        .send(backend)

      resp.body
   }

   /** Delete a curation
    *
    * @param curationId
    * @return
    */
   def delete(curationId: String): PatchResponse = {
      val request = basicRequest.auth.bearer(token)
        .patch(uri"$apiUrl/curations/$curationId/suppress")

      val resp = request
        .response(asJsonAlways[PatchResponse])
        .send(backend)

      resp.body match {
         case Left(e) =>
            throw e
         case Right(cr) =>
            cr
      }
   }
}