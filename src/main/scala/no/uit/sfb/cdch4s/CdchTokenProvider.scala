package no.uit.sfb.cdch4s

import com.github.blemale.scaffeine.Scaffeine

import scala.concurrent.duration.DurationInt
import sttp.client3._

class CdchTokenProvider(authUrl: String,
                        userName: String = sys.env("EBI_CDCH_USER"),
                        password: String = sys.env("EBI_CDCH_PASSWORD"))
                       (implicit backend: SttpBackend[Identity, Any]) {
  //The key is not used
  private val cache = Scaffeine()
    .expireAfterWrite(55.minutes) //Timeout after 1h
    .maximumSize(1) //We only need 1 token
    .build[Int, String]{ _: Int => fetchNewToken }

  private def fetchNewToken: String = {
    val request = basicRequest.auth.basic(userName, password).get(uri"$authUrl")
    val response = request.response(asString.getRight).send(backend)
    response.body
  }

  def token: String = cache.get(0)
}
