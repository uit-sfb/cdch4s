package no.uit.sfb.cdch4s

import sttp.client3.{HttpURLConnectionBackend, Identity, SttpBackend}

import java.lang.reflect.{Field, Modifier}
import java.net.HttpURLConnection

object DefaultBackend {
  /*
  Since Java's HttpURLConnection does not include PATCH in the list of available methods,
  we use reflection to add it ourselves...
  Solution found here: https://stackoverflow.com/a/39641592/4965515
   */
  try {
    val methodsField = classOf[HttpURLConnection].getDeclaredField("methods")
    methodsField.setAccessible(true)
    // get the methods field modifiers
    val modifiersField = classOf[Field].getDeclaredField("modifiers")
    // bypass the "private" modifier
    modifiersField.setAccessible(true)
    // remove the "final" modifier
    modifiersField.setInt(methodsField, methodsField.getModifiers & ~Modifier.FINAL)
    /* valid HTTP methods */ val methods = Array("GET", "POST", "HEAD", "OPTIONS", "PUT", "DELETE", "TRACE", "PATCH")
    // set the new methods - including patch
    methodsField.set(null, methods)
  } catch {
    case e@(_: SecurityException | _: IllegalArgumentException | _: IllegalAccessException | _: NoSuchFieldException) =>
      e.printStackTrace()
  }
  implicit val instance: SttpBackend[Identity, Any] = HttpURLConnectionBackend()
}
