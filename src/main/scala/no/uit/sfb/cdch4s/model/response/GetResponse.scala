package no.uit.sfb.cdch4s.model.response

case class GetResponse(totalAttributes: Long,
                       totalRecords: Long,
                       curations: Seq[CurationExt] = Seq()
                      )
