package no.uit.sfb.cdch4s.model

//Note: shortForm attribute is only used in the response
case class Evidence(identifier: Option[String] = None,
                    label: Option[String] = None
                   ) {
  assert(identifier.nonEmpty || label.nonEmpty)
}
