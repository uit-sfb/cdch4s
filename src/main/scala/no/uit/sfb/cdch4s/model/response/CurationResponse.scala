package no.uit.sfb.cdch4s.model.response

case class CurationResponse(timestamp: String,
                            curationIds: Seq[String] = Seq(),
                            errors: Seq[ErrorObj] = Seq()
                           )
