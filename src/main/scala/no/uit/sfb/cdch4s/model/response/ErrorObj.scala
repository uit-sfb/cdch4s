package no.uit.sfb.cdch4s.model.response

case class ErrorObj(errorId: String,
                    message: String,
                    recordId: Option[String] = None,
                    field: Option[String] = None,
                    attributePre: Option[String] = None,
                    attributePost: Option[String] = None,
                   )
