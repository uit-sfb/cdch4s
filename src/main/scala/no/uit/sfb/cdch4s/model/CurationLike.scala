package no.uit.sfb.cdch4s.model

trait CurationLike {
  def recordType: String
  def recordId: String
  def providerName: String
  def assertionMethod: String
  def assertionEvidences: Seq[Evidence]
  def providerUrl: Option[String]
  def dataType: Option[String]
  def dataIdentifier: Option[String]
  def attributePre: Option[String]
  def valuePre: Option[String]
  def attributePost: Option[String]
  def valuePost: Option[String]
  def attributeDelete: Boolean
  def assertionSource: Option[String]
  def assertionAdditionalInfo: Option[String]
}
