package no.uit.sfb.cdch4s.model.response

case class PatchResponse(id: String,
                         message: String,
                         success: Boolean,
                         timestamp: String
                        )
