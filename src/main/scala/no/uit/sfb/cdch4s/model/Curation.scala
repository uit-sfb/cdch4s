package no.uit.sfb.cdch4s.model

/*
                          attributePre  valuePre  attributePost valuePost attributeDelete
New attribute                                     X             X
Modify attribute name     X             X         X             =valuePre
Modify attribute value    X             X         =attributePre X
Remove attribute          X                                               true
 */
case class Curation(recordType: String,
                    recordId: String,
                    providerName: String,
                    assertionMethod: String,
                    assertionEvidences: Seq[Evidence],
                    providerUrl: Option[String] = None,
                    dataType: Option[String] = None,
                    dataIdentifier: Option[String] = None,
                    attributePre: Option[String] = None,
                    valuePre: Option[String] = None,
                    attributePost: Option[String] = None,
                    valuePost: Option[String] = None,
                    attributeDelete: Boolean = false,
                    assertionSource: Option[String] = None,
                    assertionAdditionalInfo: Option[String] = None
                   ) extends CurationLike {
  private val dataTypeValues = Seq("covid19")
  private val recordTypeValues = Seq("sample", "sequence")
  private val assertionMethodValues = Seq("automatic assertion", "manual assertion")
  assert(recordType.nonEmpty, "recordType may not be empty.")
  assert(recordId.nonEmpty, "recordId may not be empty.")
  //assert(assertionEvidences.nonEmpty, "At least one assertion evidence must be provided.")
  assert(dataType.isEmpty || dataTypeValues.contains(dataType.get), s"dataType must be one of '${dataTypeValues.mkString(", ")}'")
  assert(recordTypeValues.contains(recordType), s"recordType must be one of '${recordTypeValues.mkString(", ")}'")
  assert(assertionMethodValues.contains(assertionMethod), s"assertionMethod must be one of '${assertionMethodValues.mkString(", ")}'")
  assert(dataType.isEmpty || dataIdentifier.nonEmpty, "dataIdentifier is required when dataType is defined.")
  assert(attributePre.nonEmpty || attributePost.nonEmpty, "At least one of attributePre or attributePost must be defined.")
  assert(!(attributeDelete && (attributePost.nonEmpty || valuePost.nonEmpty)), "attributePost and valuePost must not be defined when attributeDelete is set.")
  //TODO: check that assertionSource contains a valid URI
  //TODO: check that providerUrl contains a valid URI
}
