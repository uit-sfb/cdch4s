package no.uit.sfb.cdch4s.model.response

import no.uit.sfb.cdch4s.model.{CurationLike, Evidence}

case class CurationExt(id: String,
                       submittedTimestamp: String,
                       updatedTimestamp: String,
                       recordType: String,
                       recordId: String,
                       providerName: String,
                       assertionMethod: String,
                       assertionEvidences: Seq[Evidence],
                       providerUrl: Option[String] = None,
                       dataType: Option[String] = None,
                       dataIdentifier: Option[String] = None,
                       attributePre: Option[String] = None,
                       valuePre: Option[String] = None,
                       attributePost: Option[String] = None,
                       valuePost: Option[String] = None,
                       attributeDelete: Boolean = false,
                       assertionSource: Option[String] = None,
                       assertionAdditionalInfo: Option[String] = None) extends CurationLike
