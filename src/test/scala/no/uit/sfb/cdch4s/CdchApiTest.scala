package no.uit.sfb.cdch4s

import no.uit.sfb.cdch4s.model.{Curation, Curations, Evidence}
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import no.uit.sfb.cdch4s.DefaultBackend._

class CdchApiTest extends AnyFunSpec with Matchers {
  implicit val tokenProvider = new CdchTokenProvider("https://explore.api.aai.ebi.ac.uk/auth")
  describe("CdchApi") {
    val cdchClient = new CdchApi("https://wwwdev.ebi.ac.uk/ena/clearinghouse/api")
    val recordId = "SAMN02469391"
    var curationId = ""
    it("should submit curation then retrieve it") {
      val resp = cdchClient.create(Curations(Seq(Curation(
        "sample",
        recordId,
        "MarRef",
        "manual assertion",
        Seq(Evidence(label = Some("inference based on individual clinical experience used in manual assertion"))),
        attributePre = Some("isolation source"),
        attributePost = Some("geographic location")
      ))))
      //println(resp)
      resp.errors.isEmpty should be(true)
      resp.curationIds.size should be(1)
      curationId = resp.curationIds.head
    }
    it("should retrieve curation from curationId") {
      val resp = cdchClient.getByUuid(curationId)
      //print(resp)
      resp.totalAttributes should be(1)
    }
    it("should retrieve curation from recordId") {
      val resp = cdchClient.getByRecordId(recordId)
      //print(resp)
      resp.totalRecords should be(1)
      resp.totalAttributes > 0 should be(true)
      resp.curations.nonEmpty should be(true)
    }
    it("should retrieve curation from recordId and attribute") {
      val resp = cdchClient.getByRecordId(recordId, Some("geographic location"))
      //print(resp)
      resp.totalRecords should be(1)
      resp.totalAttributes > 0 should be(true)
      resp.curations.nonEmpty should be(true)
    }
    it("should retrieve multiple curations") {
      val resp = cdchClient.fetch(limit = Some(2))
      //print(resp)
      resp.curations.nonEmpty should be(true)
    }
    it("should delete curations") {
      val resp = cdchClient.delete(curationId)
      //print(resp)
      resp.success should be(true)
    }
  }
}
