package no.uit.sfb.cdch4s

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import sttp.client3.{HttpURLConnectionBackend, Identity, SttpBackend}

import scala.util.Try

class CdchTokenProviderTest extends AnyFunSpec with Matchers {
  implicit val backend: SttpBackend[Identity, Any] = HttpURLConnectionBackend()
  describe("CdchTokenProvider") {
    it("should get a token") {
      val tokenProvider = new CdchTokenProvider("https://explore.api.aai.ebi.ac.uk/auth")
      tokenProvider.token should not be("")
    }
    it("should throw error due to wrong credentials") {
      val tokenProvider = new CdchTokenProvider("https://explore.api.aai.ebi.ac.uk/auth", "nouser", "nopassword")
      Try {
        tokenProvider.token
      }.toOption should be(None)
    }
  }
}
