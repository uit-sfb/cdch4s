# CDCH4s

Contextual Data ClearingHouse (CDCH) driver in Scala.

- [CDCH doc (PDF)](https://wwwdev.ebi.ac.uk/ena/clearinghouse/api/doc)
- [CDCH API (Swagger)](https://wwwdev.ebi.ac.uk/ena/clearinghouse/api)

## Getting started

- Create a user account at https://aai.ebi.ac.uk/registerUser (resp. https://explore.aai.ebi.ac.uk/registerUser).
- [Optional] For ease of use, save user name (resp. password) in `EBI_CDCH_USER` (resp. `EBI_CDCH_PASSWORD`) environment variable.
- Include the library in any SBT project using `libraryDependencies += "no.uit.sfb" %% "cdch4s" % "main"` (optionally replacing `main` by a specific version).  
It is of course possible to include this library in pure Java projects using Maven or Gradle.

## Usage

We first:
- import an Http backend
- create a token provider (by default, the environment variables `EBI_CDCH_USER` and `EBI_CDCH_PASSWORD` are used)
- create a cdch client instance

```scala
import no.uit.sfb.cdch4s.DefaultBackend._
implicit val tokenProvider = new CdchTokenProvider("https://explore.api.aai.ebi.ac.uk/auth")
val cdchClient = new CdchApi("https://wwwdev.ebi.ac.uk/ena/clearinghouse/api")
```

We can then submit and curations using:

```scala
cdchClient.create(...)
cdchClient.getByUuid(...)
cdchClient.getByRecordId(...)
cdchClient.fetch(...)
cdchClient.delete(...)
```