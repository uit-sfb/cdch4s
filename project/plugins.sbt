
addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.2") //Do not update to 1.9.3 as it breaks compatibility
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.10.0")
addSbtPlugin("com.gilcloud" % "sbt-gitlab" % "0.0.6")
addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "1.0.1")
