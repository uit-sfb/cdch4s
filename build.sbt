name := "cdch4s"
scalaVersion := "2.13.6"
organization := "no.uit.sfb"
organizationName := "uit-sfb"

lazy val gitCommit = settingKey[String]("Git SHA")
lazy val gitRefName = settingKey[String]("Git branch or tag")
//lazy val isRelease = settingKey[Boolean]("Is release")

useJGit

ThisBuild / gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown")}
ThisBuild / gitRefName := { sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value) }
//ThisBuild / isRelease := { sys.env.getOrElse("CI_COMMIT_REF_PROTECTED", "false").toBoolean && sys.env.getOrElse("CI_COMMIT_TAG", "").nonEmpty}
ThisBuild / version := gitRefName.value //Do not use slug here as it would replace '.' with '-'

ThisBuild / resolvers += "gitlab" at "https://gitlab.com/api/v4/groups/2067095/-/packages/maven"

val sttp = "3.4.1"
val circe = "0.14.1"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.9" % Test,
  "ch.qos.logback" % "logback-classic" % "1.2.10" % Test,
  "com.github.blemale" %% "scaffeine" % "4.0.2" % "compile", //DO NOT update 4.0.2 (conflict with akka)
  "com.softwaremill.sttp.client3" %% "core" % sttp,
  "com.softwaremill.sttp.client3" %% "circe" % sttp,
  "io.circe" %% "circe-generic" % circe
)

enablePlugins(JavaAppPackaging)

ThisBuild / gitCommit := { sys.env.getOrElse("CI_COMMIT_SHA", "unknown") }
ThisBuild / gitRefName := {
  sys.env.getOrElse("CI_COMMIT_REF_NAME", git.gitCurrentBranch.value)
}
ThisBuild / version := gitRefName.value //Do not use slug here as it would replace '.' with '-'

enablePlugins(BuildInfoPlugin)
buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion)
buildInfoPackage := s"${organization.value}.info.${name.value.replace('-', '_')}"

publishConfiguration := publishConfiguration.value.withOverwrite(true)
publishLocalConfiguration := publishLocalConfiguration.value.withOverwrite(true)